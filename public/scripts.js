var socket = io.connect('http://localhost:4000');
output = document.getElementById('test');

var graph = document.getElementById('graph');
var temperature = document.getElementById('currentTemperature');
function display(data){
  var myChart =null;
  myChart = new Chart(graph, {
    type: 'line',
    data: {
      labels: data.graphTimes,
      datasets: [
        {
          data: data.graphTemps,
          label: data.room,
          borderColor: "#3e95cd",
          fill: false
        }
      ]
    }
  });
}
function updateTemp(data){
temperature.innerHTML = data.temperature;
}

socket.on('newgraph',function(data){
  (data.graphTimes[11].includes(":00") || data.graphTimes[11].includes(":15") || data.graphTimes[11].includes(":30") || data.graphTimes[11].includes(":45")) && display(data);
  (data.graphTimes[11].includes(":05") || data.graphTimes[11].includes(":10") || data.graphTimes[11].includes(":20") || data.graphTimes[11].includes(":25")) && display(data);
  (data.graphTimes[11].includes(":35") || data.graphTimes[11].includes(":40") || data.graphTimes[11].includes(":50") || data.graphTimes[11].includes(":55")) && display(data);

  updateTemp(data);



});
