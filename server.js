var express = require("express"),
	socket = require("socket.io"),
	app = express(),
	server = app.listen(4E3, function() {
		console.log("listening on 4000")
	}),
	io = socket(server),
	data = null;
app.use(express.static('public'));
io.on("connection", function(a) {
	io.sockets.emit('newgraph', data);
	console.log("socket connection", a.id);

	a.on("incoming", function(a) {
		console.log(a);
		data = a;
		io.sockets.emit('newgraph', a)
	});
	a.on("disconnect", function() {
		console.log(a.id + " disconnected")
	})

});
